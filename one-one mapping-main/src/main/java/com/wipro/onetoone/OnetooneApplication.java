package com.wipro.onetoone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.wipro.onetoone.model.Instructor;
import com.wipro.onetoone.model.InstructorDetail;
import com.wipro.onetoone.repository.InstructorRepository;

@SpringBootApplication
public class OnetooneApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(OnetooneApplication.class, args);
	}

	@Autowired
	private InstructorRepository instructorRepository;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		Instructor instructor = new Instructor(null, "Shahjahan", "Arfin", "shahjahanarfin@gmail.com", null);

        InstructorDetail instructorDetail = new InstructorDetail(null, "Wipro Great Learning", "Basketball Player");

        instructor.setInstructorDetail(instructorDetail);

        instructorRepository.save(instructor);
	}

}
