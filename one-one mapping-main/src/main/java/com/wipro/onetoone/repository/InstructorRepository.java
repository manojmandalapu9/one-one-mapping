package com.wipro.onetoone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.onetoone.model.Instructor;

@Repository
public interface InstructorRepository extends JpaRepository<Instructor, Long> {


}
