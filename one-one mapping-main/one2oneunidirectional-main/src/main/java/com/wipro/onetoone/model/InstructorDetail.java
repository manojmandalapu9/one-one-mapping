package com.wipro.onetoone.model;

import javax.persistence.*;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "instructor_detail")
public class InstructorDetail {
	
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "id")
	    private Long id;

	    @Column(name = "youtube_channel")
	    private String youtubeChannel;

	    @Column(name = "hobby")
	    private String hobby;

}
